# Condor

An SSTO delta-winged design from TL 9 (late 21st century). Capable of getting
a few passengers and light cargo to LEO and return without staging or refuelling.
Uses four Aerospike engines, and most of the wing volume is Methalox fuel tank.

## Licenses

Earth and cloud textures courtesy of NASA.

https://visibleearth.nasa.gov/images/57747/blue-marble-clouds
https://visibleearth.nasa.gov/images/73580/january-blue-marble-next-generation-w-topography-and-bathymetry
