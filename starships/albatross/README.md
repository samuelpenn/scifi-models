# Albatross Light Freighter

85dt TL 9 light freighter. Chemical drive with Liquid Oxygen/Methane propellant.

## Notes

Cargo bay isn't quite sized for efficiently storing standard cargo containers. Fits 4 with plenty of room, but no more. Might be nice to resize so it fits 6 (or 8).

Airlock is 'beneath' ship so can dock and have plenty of space to open the cargo bay doors.

Need to finalise crew quarters and storage for supplies. Supplies could be in front of cargo bay, with access through the neck.

## Performance

Estimated performance: 8km/s empty, 3.2km/s with maximum load.

## Licenses

Albatross image (CC BY):
https://www.flickr.com/photos/blachswan/44921677341
