# Emu

Small (~20dt) tug/maintenance craft. 3 crew for short duration flights.
Arms for fixing/manipulating objects. Distributed engines so can be used
to tug things without burning them (can be angled).

Post-hoc meaning of name was Engineering & Maintenance Unit
