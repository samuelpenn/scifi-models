# Swallow

Outer system freighter, nuclear powered. TL 9, ~470dt.

Front half detaches and moves under chemical power. Main nuclear drive is pushed to the limits of this technology, and is not very clean.

## Performance

Estimated: 13km/s

With no methane fuel: 21km/s

Performance of chemical drive on fore shuttle: 3km/s
