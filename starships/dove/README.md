# Dove Transport

Transport designed for in-orbit transport and to/from surfaces of small Moons.

## Third Party

Painted Metal textures under CC0 license from https://cc0textures.com/view?id=PaintedMetal002
