# Robin

Small orbital transport. Designed for moving goods between stations and
ships in a planetary orbit.

25dT

Fuel: 5dT

Mono Fuel: 1dT

Cargo: 10dT

Crew: 6dT

Dry Mass: 11,500kg

Wet Mass: 81,500kg

Accl: 12m/s/s

Delta-v: 6.46km/s (no cargo)

Cargo: +80,000kg

Delta-v: 1.87km/s (max cargo)


RCS

Accl with load: .6
Accl without load: 1.2

Delta-vee: 300m/s


## Textures

Enceladus image courtesy of NASA/JPL-Caltech/Space Science Institute/Lunar and Planetary Institute:
https://www.jpl.nasa.gov/spaceimages/details.php?id=PIA18435
