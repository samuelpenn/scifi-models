# Falcon Heavy Lifter

TL: 9 (Late 20th Century)
Tonnage: 151dT

Designed to lift loads into orbit, capable of taking second stage into high Earth orbit.

Both first and second stage capable of landing on Earth, Moon, Mars or smaller bodies,
depending on fuel load.

*Boost stage* can get the *Crew stage* into a high Earth orbit, and *Crew Stage* can then get from there to Mars orbit.

## Performance

### Boost Stage (130dT)

Chem engines (x21): 21dT
Fuel: 100dt

Body type: Weak (x0.75)
Dry Mass: 210 + 15 = 225t
Wet Mass: 225t + 1000t = 1225t

#### Sea level:
  * Impulse: 3.5
  * Thrust: 63,000
  * Accel: 51m/s² -> 235m/s²
  * dV: 6km/s

#### Sea level (with crew stage):

Dry Mass: 223t + 266t = 489t
Wet Mass: 933t + 266t = 1199t

  * Accel: 4.3m/s² -> 10m/s²

### Crew stage (38dt)

Aerospike: 3dT 
Fuel: 20dt
Crew: 10dT
Cargo: 5dT

Body Type: Weak (x0.75)
Dry Mass: (15 + 2) + 30 = 43t
Wet Mass: 43 + 200 = 243t

#### Vacuum:
  * Impulse: 4.2
  * Thrust: 2000
  * Accel: 7.5m/s² -> 30m/s²
  * dV: 5.8km/s

#### Sea level:
  * Impulse: 3.7
  * Thrust: 2000
  * Accel: 7.5m/s² -> 30m/s²
  * dV: 5.2km/s

Not enough thrust to take off on full fuel load, but can land with aerobraking.




