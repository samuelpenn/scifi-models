# Vargr Yacht

150dT Vargr star yacht. Designed for speed and luxury.

J-3 drive, with Thrust 4. TL 14.

150dT

## Hull

Closed Structure
Hull Points: 66
Cost: 6.75MCr

Bonded Superdense (+5) 4t, 2.7MCr
Reflec Armour (+3 v Lasers) 15MCr

Emissions Absorption Grid 3t 6MCr (DM -2 on sensors)

## Drives

Thrust 4
4% Hull: 6t
Jump-2: 8t

## Power Plant

Needed: 30 + 60 + 60 = 150

Power Plant: Fusion (TL 12)
12t, 12MCr = 180

## Fuel Tanks

Jump Drive: 45t

Power Plant: 18dt (4 weeks)

## Bridge

10t, 1MCr

## Computer

Computer/30, 20MCr

## Sensors

Civilian, 1t, 1Pow, 3MCr

## Weapons

## Optional Systems

## Staterooms

5 staterooms = 20t



