# Science Fiction Models

Some 3D models and sketches for SciFi models, mostly spacecraft and related
things. Currently the models are being done in Blender, a tool with which I
have almost zero experience, so don't expect anything of high quality at this
stage.


## References

For the plausible future designs, I've tried to base them on actual real world
designs, at least for things like basic engines etc.

Information on Aerospikes, plus images of Vacuum v Sea Level designs for Merlin:

https://everydayastronaut.com/aerospikes/


There're some images of Raptor-SL v Raptor-Vac here, along with Merlin 1D:

https://www.reddit.com/r/spacex/comments/55il7y/3d_model_of_the_raptor_engine/

