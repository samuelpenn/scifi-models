# Textures

Some of these textures are taken from the Pixar One Twenty Eight free texture library.

https://renderman.pixar.com/pixar-one-twenty-eight

Pixar One Twenty Eight by Pixar Animation Studios is licensed under a Creative Commons
Attribution 4.0 International License.

