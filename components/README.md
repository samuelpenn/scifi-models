# Components

This is a collection of components that will get re-used in multiple
ship designs. Currently, this is engines.

## RD-4

TL 9 0.25 dT engine, for both sealevel and vacuum.

SeaLevel has expansion ratio of about 36.
Vacuum variant has expansion ratio of about 150.

The SeaLevel variant has optional vectoring.
